import 'package:flutter/material.dart';

// Reference Documents:
// GridView: GridView class
//   - https://docs.flutter.io/flutter/widgets/GridView-class.html
//   - https://camposha.info/flutter/gridview

// GridView
class GridViewScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("GridView"),
      ),
      body: GridViewBody(),
    );
  }
}

class GridViewBody extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return GridViewState();
  }
}

class GridViewState extends State<GridViewBody> {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 4,
      ),
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          child: Card(
            elevation: 5.0,
            child: Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(
                top: 10.0,
                bottom: 10.0,
              ),
              child: Center(
                child: Text('avatar'),
              ),
            ),
          ),
          onTap: () {
            print("tap");
          },
        );
      },
      itemCount: 100,
    );
  }
}

import 'dart:convert';
import 'dart:async' show Future;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;

// Reference Documents:
// Icon: Icon class
//   - https://docs.flutter.io/flutter/widgets/Icon-class.html

// Icon
class IconPage extends StatefulWidget {
  static const String routeName = '/info/icon';

  @override
  State<StatefulWidget> createState() {
    return _IconPageState();
  }
}

class _IconPageState extends State<IconPage> {
  _IconPageState() {
    loadJson();
  }
  static final List<MaterialColor> iconColors = <MaterialColor>[
    Colors.red,
    Colors.pink,
    Colors.purple,
    Colors.deepPurple,
    Colors.indigo,
    Colors.blue,
    Colors.lightBlue,
    Colors.cyan,
    Colors.teal,
    Colors.green,
    Colors.lightGreen,
    Colors.lime,
    Colors.yellow,
    Colors.amber,
    Colors.orange,
    Colors.deepOrange,
    Colors.brown,
    Colors.grey,
    Colors.blueGrey,
  ];
  int iconColorIndex = 6; // light blue
  IconsListModel iconsData = IconsListModel(
    icons: [],
  );

  Future loadJson() async {
    String jsonString = await rootBundle.loadString(
        'assets/data/material_icons.json'
    );
    final jsonObj = json.decode(jsonString);
    setState(() {
      iconsData = IconsListModel.fromJson(jsonObj);
    });
  }

  void iconPressed() {
    setState(() {
      iconColorIndex = (iconColorIndex + 1) % iconColors.length;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Icon'),
      ),
      body: SafeArea(
          top: false,
          bottom: false,
          child: IconTheme(
            data: IconThemeData(color: iconColors[iconColorIndex]),
            child: ListView(
            padding: const EdgeInsets.all(8.0),
            children: iconsData.icons.map<Widget>((IconModel icon) {
              return _IconsCard(
                icon: icon,
                iconPressed: iconPressed,
              );
            }).toList(),
          ),
        ),
      ),
    );
  }
}

class IconModel {
  IconModel(this.name, this.codePoint);
  final String name;
  final int codePoint;

  factory IconModel.fromJson(Map<String, dynamic> parsedJson) {
    return IconModel(
      parsedJson['name'],
      int.tryParse(parsedJson['code_point']),
    );
  }
}

class IconsListModel {
  IconsListModel({
    this.icons,
  });
  final List<IconModel> icons;

  factory IconsListModel.fromJson(List<dynamic> parsedJson) {
    List<IconModel> icons = new List<IconModel>();
    icons = parsedJson.map((item)=>IconModel.fromJson(item)).toList();
    return new IconsListModel(
      icons: icons,
    );
  }
}

class _IconsCard extends StatelessWidget {
  const _IconsCard({
    this.icon,
    this.iconPressed
  });
  final IconModel icon;
  final VoidCallback iconPressed;

  Widget _centeredText(String label) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(label, textAlign: TextAlign.center),
    );
  }

  TableRow _headerRow() {
    return TableRow(
      children: <Widget>[
        _centeredText('Size'),
        _centeredText('Enabled'),
        _centeredText('Disabled'),
      ],
    );
  }

  TableRow _iconRow(double size) {
    return TableRow(
      children: <Widget>[
        _centeredText(size.floor().toString()),
        IconButton(
          icon: Icon(IconData(icon.codePoint, fontFamily: 'MaterialIcons')),
          iconSize: size,
          tooltip: "Enabled icon button",
          onPressed: iconPressed,
        ),
        IconButton(
          icon: Icon(IconData(icon.codePoint, fontFamily: 'MaterialIcons')),
          iconSize: size,
          tooltip: "Disabled icon button",
          onPressed: null,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final TextStyle titleStyle = theme.headline.copyWith(
      color: theme.headline.color,
    );
    final TextStyle textStyle = theme.subhead.copyWith(
      color: theme.caption.color,
    );
    return Card(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 12.0),
            child: DefaultTextStyle(
              softWrap: false,
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.subhead,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Text(icon.name,
                      style: titleStyle,
                    ),
                  ),
                ],
              ),
            ),
          ),
          DefaultTextStyle(
            style: textStyle,
            child: Table(
              defaultVerticalAlignment: TableCellVerticalAlignment.middle,
              children: <TableRow>[
                _headerRow(),
                _iconRow(18.0),
                _iconRow(24.0),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_demo/base/files.dart';

// Reference Documents:
// Image: Image class
//   - https://docs.flutter.io/flutter/widgets/Image-class.html
//   - https://flutterdoc.com/widgets-image-5a2296493329
//   - https://flutter.io/docs/cookbook/persistence/reading-writing-files
//   - https://flutter.io/docs/cookbook/images/cached-images
//   - https://flutter.io/docs/cookbook/persistence/reading-writing-files

// Image
class ImagePage extends StatefulWidget {
  static const String routeName = '/info/image';

  @override
  State<StatefulWidget> createState() {
    return _ImagePageState();
  }
}

class _ImagePageState extends State<ImagePage> {
  final imgPath = 'google_logo.jpg';
  
  _ImagePageState() {
    // Load images
    rootBundle.load('assets/images/googlelogo_color_272x92dp.png')
        .then((ByteData dat) {
      var imgObj = FilesStorage().getLocalFile(imgPath);
      if (imgObj != null) {
        print('ImagePage is initializing photo file.');
        imgObj.writeAsBytes(dat.buffer.asUint8List());
      }
      // Init members
      images = [
        ImageModel(
          title: 'new Image.asset',
          subtitle: 'for obtaining an image from an AssetBundle using a key.',
          loadImage: () {
            return Image.asset('assets/images/googlelogo_color_272x92dp.png');
          },
        ),
        ImageModel(
          title: 'new Image.network',
          subtitle: 'for obtaining an image from a URL.',
          loadImage: () {
            return Image.network(
                'https://www.google.com/images/branding/googlelogo'
                    '/1x/googlelogo_color_272x92dp.png');
          },
        ),
        ImageModel(
          title: 'new Image.file',
          subtitle: 'for obtaining an image from a File.',
          loadImage: () {
            return Image.file(FilesStorage().getLocalFile(imgPath));
          },
        ),
        ImageModel(
          title: 'new Image.memory',
          subtitle: 'for obtaining an image from a Uint8List.',
          loadImage: () {
            return Image.memory(dat.buffer.asUint8List());
          },
        ),
      ];
      setState(() {
      });
    });
  }
  List<ImageModel> images = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Image'),
      ),
      body: SafeArea(
        top: false,
        bottom: false,
        child: ListView(
          children: images.map<Widget>((ImageModel img) {
            return _ImageCard(
              data: img,
            );
          }).toList(),
        ),
      ),
    );
  }
}

class ImageModel {
  ImageModel({
    this.title,
    this.subtitle,
    this.loadImage,
  });

  String title;
  String subtitle;
  Function loadImage;
}

class _ImageCard extends StatefulWidget {
  const _ImageCard({
    this.data,
  });
  final ImageModel data;

  @override
  State<StatefulWidget> createState() {
    return _ImageCardState();
  }
}

class _ImageCardState extends State<_ImageCard> {
  Image image;
  bool isButtonEnabled;

  @override
  void initState() {
    image = Image.asset('assets/images/info-icon.png');
    isButtonEnabled = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme textTheme = Theme.of(context).textTheme;
    final TextStyle titleStyle = textTheme.headline;
    final TextStyle descStyle = textTheme.subhead;
    return Container(
      padding: const EdgeInsets.all(8.0),
      height: 350.0,
      child: Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 180.0,
              child: Stack(
                children: <Widget>[
                  Positioned.fill(
                    child: this.image,
                  ),
                  Positioned(
                    bottom: 16.0,
                    left: 16.0,
                    right: 16.0,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      alignment: Alignment.centerLeft,
                      child: Text(widget.data.title,
                        style: titleStyle.copyWith(color: Colors.white),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0.0),
                child: DefaultTextStyle(
                  softWrap: false,
                  overflow: TextOverflow.ellipsis,
                  style: descStyle,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: Text(widget.data.title,
                          style: descStyle.copyWith(color: Colors.black54),
                        ),
                      ),
                      Text(widget.data.subtitle),
                    ],
                  ),
                ),
              ),
            ),
            ButtonTheme.bar(
              child: ButtonBar(
                alignment: MainAxisAlignment.start,
                children: <Widget>[
                  FlatButton(
                    child: Text(isButtonEnabled ? 'LOAD' : 'LOADED',
                      semanticsLabel: 'Load photo',
                    ),
                    textColor: Colors.blueAccent.shade400,
                    onPressed: !isButtonEnabled ? null : () {
                      setState(() {
                        isButtonEnabled = false;
                        var imgLoaded = widget.data.loadImage();
                        if (imgLoaded != null) {
                          image = imgLoaded;
                          return;
                        }
                        isButtonEnabled = true;
                        print('Load failed');
                      });
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

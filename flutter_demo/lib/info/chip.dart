import 'package:flutter/material.dart';

// Reference Documents:
// Icon: Chip class
//   - https://docs.flutter.io/flutter/material/Chip-class.html

// Chip
class ChipPage extends StatefulWidget {
  static const String routeName = '/info/chip';

  @override
  State<StatefulWidget> createState() {
    return _ChipPageState();
  }
}

class _ChipPageState extends State<ChipPage> {
  _ChipPageState() {
    _reset();
  }
  _reset() {
    // Clear containers
    _chips.clear();
    _inputChips.clear();
    _choiceChips.clear();
    _filterChips.clear();
    _chips.addAll(_demoChips);
    _inputChips.addAll(_demoChips);
    _choiceChips.addAll(_demoChips);
    _filterChips.addAll(_demoChips);
    // Clear selected
    _selectedMaterial = '';
    _selectedInputChips.clear();
    _selectedFilterChips.clear();
  }
  // Containers
  final Set<String> _chips = Set<String>();
  final Set<String> _inputChips = Set<String>();
  final Set<String> _choiceChips = Set<String>();
  final Set<String> _filterChips = Set<String>();
  // Selected
  String _selectedMaterial = '';
  final Set<String> _selectedInputChips = Set<String>();
  final Set<String> _selectedFilterChips = Set<String>();

  @override
  Widget build(BuildContext context) {
    // Build for tiles
    final List<Widget> tiles = <Widget>[
      const SizedBox(height: 8.0, width: 8.0),
      // class Chip
      _ChipsTile(
        label: 'Remove Cities (Chip)',
        children: _chips.map<Widget>((String name) {
          return Chip(
            key: ValueKey<String>(name),
            backgroundColor: Colors.blue.shade300,
            label: Text(name),
            onDeleted: () {
              setState(() {
                _chips.remove(name);
                if (_selectedMaterial == name) {
                  _selectedMaterial = '';
                }
              });
            },
          );
        }).toList(),
      ),
      // class InputChip
      _ChipsTile(
        label: 'Remove Cities (InputChip)',
        children: _inputChips.map<Widget>((String name) {
          return InputChip(
            key: ValueKey<String>(name),
            avatar: CircleAvatar(
              backgroundImage: AssetImage('assets/images/info-card.png'),
            ),
            label: Text(name),
            onDeleted: () {
              setState(() {
                _inputChips.remove(name);
                _selectedInputChips.remove(name);
              });
            },
          );
        }).toList(),
      ),
      // ChoiceChip
      _ChipsTile(
        label: 'Choose a City (ChoiceChip)',
        children: _choiceChips.map<Widget>((String name) {
          return ChoiceChip(
            key: ValueKey<String>(name),
            backgroundColor: Colors.blue.shade300,
            label: Text(name),
            selected: _selectedMaterial == name,
            onSelected: (bool value) {
              setState(() {
                _selectedMaterial = value ? name : '';
              });
            },
          );
        }).toList(),
      ),
      // FilterChip
      _ChipsTile(
        label: 'Choose Cities (FilterChip)',
        children: _filterChips.map<Widget>((String name) {
          return FilterChip(
            key: ValueKey<String>(name),
            label: Text(name),
            selected: _filterChips.contains(name) ?
              _selectedFilterChips.contains(name) : false,
            onSelected: !_filterChips.contains(name) ?
              null : (bool value) {
                setState(() {
                  if (!value) {
                    _selectedFilterChips.remove(name);
                  } else {
                    _selectedFilterChips.add(name);
                  }
                });
              },
          );
        }).toList(),
      ),
      const Divider(),
    ];
    return Scaffold(
      appBar: AppBar(
        title: Text('Chip'),
      ),
      body: ChipTheme(
        data: Theme.of(context).chipTheme,
        child: ListView(children: tiles),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.refresh,
          semanticLabel: 'Reset chips',
        ),
        onPressed: () {
          setState(() {
            _reset();
          });
        },
      ),
    );
  }
}

class _ChipsTile extends StatelessWidget {
  const _ChipsTile({
    Key key,
    this.label,
    this.children,
  }) : super(key: key);

  final String label;
  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    final List<Widget> cardChildren = <Widget>[
      Container(
        padding: const EdgeInsets.only(top: 16.0, bottom: 4.0),
        alignment: Alignment.center,
        child: Text(label, textAlign: TextAlign.start),
      ),
    ];

    if (children.isNotEmpty) {
      cardChildren.add(Wrap(
        children: children.map<Widget>((Widget chip) {
          return Padding(
            padding: const EdgeInsets.all(2.0),
            child: chip,
          );
        }).toList(),
      ));
    } else {
      cardChildren.add(
        Semantics(
          container: true,
          child: Container(
            alignment: Alignment.center,
            constraints: const BoxConstraints(
              minWidth: 48.0,
              minHeight: 48.0,
            ),
            padding: const EdgeInsets.all(8.0),
            child: Text('None',
              style: Theme.of(context).textTheme.caption.copyWith(
                  fontStyle: FontStyle.italic,
              )
            ),
          ),
        )
      );
    }

    return Card(
      semanticContainer: false,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: cardChildren,
      ),
      color: Colors.blue.shade50,
    );
  }
}

const List<String> _demoChips = <String>[
  'osaka',
  'kyoto',
  'fujiyama',
  'tokyo',
];

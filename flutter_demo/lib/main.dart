import 'package:flutter/material.dart';
import 'package:flutter_demo/base/files.dart';
import 'package:flutter_demo/info/image.dart';
import 'package:flutter_demo/info/icon.dart';
import 'package:flutter_demo/info/chip.dart';
import 'package:flutter_demo/info/grid_view.dart';
import 'package:flutter_demo/layout/list_tile.dart';
import 'package:flutter_demo/layout/stepper.dart';
import 'package:flutter_demo/layout/divider.dart';

void main() {
  FilesStorage().load();
  runApp(MaterialApp(
    title: 'Flutter Demo',
    home: HomeScreen(),
  ));
}

// Material
// https://flutter.io/docs/development/ui/widgets/material

// Animations
// https://flutter.io/docs/development/ui/animations/tutorial

// Home Screen
// https://docs.flutter.io/flutter/material/ListTile-class.html
class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Welcome to Flutter'),
      ),
      body: SafeArea(
        top: true,
        bottom: true,
        child: ListView.builder(
          itemCount: homeList.length,
          itemBuilder: (BuildContext context, int index) {
            var builder = homeList[index];
            return builder.build(context);
          },
        ),
      ),
    );
  }
}

abstract class HomeListTileBuilder {
  ListTile build(context);
}

class HomeHeader extends HomeListTileBuilder {
  HomeHeader({
    this.title,
  });

  String title;

  @override
  ListTile build(context) {
    return ListTile(
      title: Text(this.title),
      dense: true,
    );
  }
}

class HomeItem extends HomeListTileBuilder {
  HomeItem({
    this.title,
    this.subtitle,
    this.image,
    this.screen,
  });

  String title;
  String subtitle;
  String image;
  Widget screen;
  Image cachedImage = null;

  @override
  ListTile build(context) {
    if (cachedImage == null) {
      cachedImage = Image.asset(image,
        width: 50,
        height: 50,
      );
    }
    return ListTile(
      leading: cachedImage,
      title: Text(title),
      subtitle: Text(subtitle),
      onTap: () {
        if (screen != null) {
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => screen));
        }
      },
    );
  }
}

var homeList = [
  HomeHeader(
    title: 'Information displays',
  ),
  HomeItem(
    title: 'Image',
    subtitle: 'A widget that displays an image.',
    image: 'assets/images/info-image.jpg',
    screen: ImagePage(),
  ),
  HomeItem(
    title: 'Icon',
    subtitle: 'A Material Design icon.',
    image: 'assets/images/info-icon.png',
    screen: IconPage(),
  ),
  HomeItem(
    title: 'Chip',
    subtitle: 'A Material Design chip. Chips represent complex entities in '
        'small blocks, such as a contact.',
    image: 'assets/images/info-chip.png',
    screen: ChipPage(),
  ),
//  HomeItem(
//    title: 'Tooltip',
//    subtitle: 'Tooltips provide text labels that help explain the function of '
//        'a button or other user interface action. Wrap the button in a Tooltip '
//        'widget to show a label when the widget long pressed (or when the user '
//        'takes some other appropriate action).',
//    image: 'assets/images/info-tooltip.png',
//    screen: null,
//  ),
//  HomeItem(
//    title: 'DataTable',
//    subtitle: 'Data tables display sets of raw data. They usually appear in '
//        'desktop enterprise products. The DataTable widget implements this '
//        'component.',
//    image: 'assets/images/info-datatable.png',
//    screen: null,
//  ),
//  HomeItem(
//    title: 'Card',
//    subtitle: 'A Material Design card. A card has slightly rounded corners and '
//        'a shadow.',
//    image: 'assets/images/info-card.png',
//    screen: null,
//  ),
//  HomeItem(
//    title: 'LinearProgressIndicator',
//    subtitle: 'A material design linear progress indicator, also known as a '
//        'progress bar.',
//    image: 'assets/images/info-linearprogress.png',
//    screen: null,
//  ),
//  HomeItem(
//    title: 'CircularProgressIndicator',
//    subtitle: 'A material design circular progress indicator, which spins to '
//        'indicate that the application is busy.',
//    image: 'assets/images/info-circleprogress.png',
//    screen: null,
//  ),
  HomeItem(
    title: 'GridView',
    subtitle: 'A grid list consists of a repeated pattern of cells arrayed in '
        'a vertical and horizontal layout. The GridView widget implements this '
        'component.',
    image: 'assets/images/info-gridview.png',
    screen: GridViewScreen(),
  ),
  HomeHeader(
    title: 'Layout',
  ),
  HomeItem(
    title: 'ListTile',
    subtitle: 'A single fixed-height row that typically contains some text as '
        'well as a leading or trailing icon.',
    image: 'assets/images/layout-lists.png',
    screen: ListTilePage(),
  ),
  HomeItem(
    title: 'Stepper',
    subtitle: 'A Material Design stepper widget that displays progress through '
        'a sequence of steps.',
    image: 'assets/images/layout-stepper.png',
    screen:StepperPage(),
  ),
  HomeItem(
    title: 'Divider',
    subtitle: 'A one logical pixel thick horizontal line, with padding on '
        'either side.',
    image: 'assets/images/layout-divider.png',
    screen: DividerPage(),
  ),
];

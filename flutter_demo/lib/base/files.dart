import 'dart:io';
import 'package:path_provider/path_provider.dart';

class FilesStorage {
  // Singleton
  static final FilesStorage _singleton = new FilesStorage._internal();
  factory FilesStorage() {
    return _singleton;
  }
  FilesStorage._internal();

  load() async {
    print('FilesStorage is loading...');
    localPath = await getApplicationDocumentsDirectory();
    print('FilesStorage Path: $localPath');
  }

  // Files
  Directory localPath;

  File getLocalFile(String path) {
    if (localPath == null) {
      return null;
    }
    var homePath = localPath.path;
    return File('$homePath/$path');
  }
}

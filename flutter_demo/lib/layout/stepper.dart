import 'package:flutter/material.dart';

// Reference Documents:
// Stepper: Stepper class
//   - https://docs.flutter.io/flutter/material/Stepper-class.html
// Stepper: Steppers and Form is flutter
//   - https://medium.com/flutterpub/steppers-and-form-in-flutter-49cda857230c
// Stepper: Example Code
//   - https://github.com/AseemWangoo/flutter_programs/blob/master/StepperWithForm.dart
// FocusNode: Focus on a Text Field
//   - ref: https://flutter.io/docs/cookbook/forms/focus

// Stepper
class StepperData {
  String name = '';
  String phone = '';
  String email = '';
}

// Stepper Screen
class StepperPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Stepper'),
      ),
      body: StepperBody(),
    );
  }
}

// Stepper Body
class StepperBody extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _StepperState();
  }
}

class _StepperState extends State<StepperBody> {
  var curStep = 0;
  var formKey = GlobalKey<FormState>();
  var focusNode = FocusNode();
  static var data = StepperData();

  List<Step> steps = [
    Step(
      title: Text('Name'),
      isActive: true,
      state: StepState.indexed,
      content: TextFormField(
        keyboardType: TextInputType.text,
        autocorrect: false,
        maxLines: 1,
        validator: (value) {
          if (value.isEmpty || value.length < 1) {
            return 'Please enter name';
          }
        },
        onSaved: (String value) {
          data.name = value;
        },
        decoration: InputDecoration(
          labelText: 'Enter your name',
          hintText: 'Enter a name',
          icon: const Icon(Icons.person),
          labelStyle: TextStyle(
            decorationStyle: TextDecorationStyle.solid,
          ),
        ),
      ),
    ),
    Step(
      title: Text('Phone'),
      isActive: true,
      state: StepState.indexed,
      content: TextFormField(
        keyboardType: TextInputType.phone,
        autocorrect: false,
        maxLines: 1,
        validator: (value) {
          if (value.isEmpty || value.length < 10) {
            return 'Please enter valid phone number';
          }
        },
        onSaved: (String value) {
          data.phone = value;
        },
        decoration: InputDecoration(
          labelText: 'Enter your phone number',
          hintText: 'Enter a phone number',
          icon: const Icon(Icons.phone),
          labelStyle: TextStyle(
            decorationStyle: TextDecorationStyle.solid,
          ),
        ),
      ),
    ),
    Step(
      title: Text('Email'),
      isActive: true,
      state: StepState.indexed,
      content: TextFormField(
        keyboardType: TextInputType.emailAddress,
        autocorrect: false,
        maxLines: 1,
        validator: (value) {
          if (value.isEmpty || !value.contains('@')) {
            return 'Please enter valid email';
          }
        },
        onSaved: (String value) {
          data.email = value;
        },
        decoration: InputDecoration(
          labelText: 'Enter your email',
          hintText: 'Enter a email address',
          icon: const Icon(Icons.email),
          labelStyle: TextStyle(
            decorationStyle: TextDecorationStyle.solid,
          ),
        ),
      ),
    ),
  ];

  @override
  void initState() {
    super.initState();
    focusNode.addListener(() {
      setState(() {
      });
      print('Has focus: $focusNode.hasFoucs');
    });
  }

  @override
  void dispose() {
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    void submitDetails() {
      final FormState formState = formKey.currentState;
      if (!formState.validate()) {
        Scaffold.of(context).showSnackBar(SnackBar(
            content: Text('Please enter correct data'))
        );
      }
    }

    return Container(
      child: Form(
        key: formKey,
        child: ListView(
          children: <Widget>[
            Stepper(
              steps: steps,
              type: StepperType.vertical,
              currentStep: curStep,
              onStepContinue: () {
                setState(() {
                  if (curStep < steps.length - 1) {
                    curStep = curStep + 1;
                  } else {
                    curStep = 0;
                  }
                });
              },
              onStepCancel: () {
                setState(() {
                  if (curStep > 0) {
                    curStep = curStep - 1;
                  } else {
                    curStep = 0;
                  }
                });
              },
              onStepTapped: (step) {
                setState(() {
                  curStep = step;
                });
              },
            ),
            RaisedButton(
              child: Text(
                'Save details',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              onPressed: submitDetails,
              color: Colors.blue,
            ),
          ],
        ),
      ),
    );
  }
}

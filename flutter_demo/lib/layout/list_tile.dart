import 'package:flutter/material.dart';

// Reference Documents:
// ListTile: ListTile class
//   - https://docs.flutter.io/flutter/material/ListTile-class.html

// ListTile
class ListTilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('List'),
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
            leading: Text('avatar'),            // 头部部件
            title: Text('List Tile'),           // 标题部件
            subtitle: Text('Subtitle'),         // 副标题部件
            trailing: Text('doc'),              // 尾部部件
            isThreeLine: true,                  // 是否三行
            dense: false,                       // 是否稠密
            contentPadding: EdgeInsets.all(20), // 内容内边距
            enabled: true,                      // 是否启用
            onTap: () {},                       // 点击时
            onLongPress: () {},                 // 长按时
            selected: false,                    // 被选定
          ),
          ListTile(
            title: Text('App Bar'),
          )
        ]
      ),
    );
  }
}

class DetailScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detail'),
      ),
    );
  }
}
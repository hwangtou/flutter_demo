import 'package:flutter/material.dart';

// Reference Documents:
// Divider: Divider class
//   - https://docs.flutter.io/flutter/material/ListTile-class.html

// Divider
class DividerPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _DividerPageState();
  }
}

class _DividerPageState extends State<DividerPage> {
  final List<DividerListTileBuilder> homeList = [
    DividerHeader(
      title: 'Information displays',
    ),
    DividerItem(
      title: 'Image',
      subtitle: 'A widget that displays an image.',
      image: 'assets/images/info-image.jpg',
    ),
    DividerItem(
      title: 'Icon',
      subtitle: 'A Material Design icon.',
      image: 'assets/images/info-icon.png',
    ),
    DividerItem(
      title: 'Chip',
      subtitle: 'A Material Design chip. Chips represent complex entities in '
          'small blocks, such as a contact.',
      image: 'assets/images/info-chip.png',
    ),
    DividerHeader(
      title: 'Layout',
    ),
    DividerItem(
      title: 'ListTile',
      subtitle: 'A single fixed-height row that typically contains some text as '
          'well as a leading or trailing icon.',
      image: 'assets/images/layout-lists.png',
    ),
    DividerItem(
      title: 'Stepper',
      subtitle: 'A Material Design stepper widget that displays progress through '
          'a sequence of steps.',
      image: 'assets/images/layout-stepper.png',
    ),
    DividerItem(
      title: 'Divider',
      subtitle: 'A one logical pixel thick horizontal line, with padding on '
          'either side.',
      image: 'assets/images/layout-divider.png',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Welcome to Flutter'),
      ),
      body: ListView.separated(
        separatorBuilder: (context, index) => Divider(
          color: Colors.blue,
        ),
        itemCount: homeList.length,
        itemBuilder: (BuildContext context, int index) {
          var builder = homeList[index];
          return builder.build(context);
        },
      ),
    );
  }
}

abstract class DividerListTileBuilder {
  ListTile build(context);
}

class DividerHeader extends DividerListTileBuilder {
  DividerHeader({
    this.title,
  });

  String title;

  @override
  ListTile build(context) {
    return ListTile(
      title: Text(this.title),
      dense: true,
    );
  }
}

class DividerItem extends DividerListTileBuilder {
  DividerItem({
    this.title,
    this.subtitle,
    this.image,
    this.screen,
  });

  String title;
  String subtitle;
  String image;
  Widget screen;

  @override
  ListTile build(context) {
    return ListTile(
      leading: Image(
        image: AssetImage(image),
        width: 60,
        height: 60,
      ),
      title: Text(title),
      subtitle: Text(subtitle),
      onTap: () {
        if (screen != null) {
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => screen));
        }
      },
    );
  }
}

import os, sys, requests, re, json
from bs4 import BeautifulSoup

reload(sys)
sys.setdefaultencoding('utf-8')

def main():
    url = "https://docs.flutter.io/flutter/material/Icons-class.html"
    r = requests.get(url)
    soup = BeautifulSoup(r.text, 'html.parser')
    constants = soup.find('section', {'id': 'constants'})
    objs = []
    for icon_tag in constants.dl.find_all('dd'):
        # get code
        code_tag = icon_tag.code
        codes = re.findall(r'(0x[0-9a-fA-F]+)', str(code_tag))
        code = codes[0]
        # get name 
        names = re.findall(r'named "(.*)"', str(icon_tag))
        name = names[0]
        objs.append({
            "name": name,
            "code_point": code,
        })
    # dump to json
    json_path = '../assets/data'
    if not os.path.exists(json_path):
        os.makedirs(json_path)
    with open(json_path + '/material_icons.json', 'w') as outfile:
        json.dump(objs, outfile)


if __name__ == "__main__":
    main()

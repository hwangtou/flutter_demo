Flutter

#Flutter Material库

##Scaffold(脚手架)类

官方原文链接：[https://docs.flutter.io/flutter/material/Scaffold-class.html](https://docs.flutter.io/flutter/material/Scaffold-class.html)

实现了基本的Material Design的视觉布局结构。

这给类提供了[Navigation Drawer（导航侧栏）](https://material.io/design/components/navigation-drawer.html)、[Snack Bars（消息底部提醒）](https://material.io/design/components/snackbars.html)和[Bottom Sheet（底部栏）](https://material.io/design/components/sheets-bottom.html)展示的API。

![](https://storage.googleapis.com/spec-host-backup/mio-design%2Fassets%2F1nsuL8VDpBW_LZYXgabK1H0uq6icmmKYt%2Fnav-drawer-intro.png)
